/*
 * egl-no-error-example
 * Copyright 2021 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <X11/Xlib.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>

static bool
test_context(EGLDisplay egl_display, EGLConfig config,
             EGLint major, EGLint minor)
{
  const EGLint ctx_attribs[] = {
    EGL_CONTEXT_MAJOR_VERSION_KHR, major,
    EGL_CONTEXT_MINOR_VERSION_KHR, minor,
    EGL_CONTEXT_OPENGL_NO_ERROR_KHR, EGL_FALSE,
    EGL_NONE
  };

  EGLContext ctx = eglCreateContext(egl_display, config,
                                    EGL_NO_CONTEXT, ctx_attribs);
  if (!ctx) {
    printf("ERROR: eglCreateContext failed for GLES version %d.%d.\n",
           major, minor);
    return false;
  }

  eglDestroyContext(egl_display, ctx);

  return true;
}

int
main()
{
  Display *x11_display = XOpenDisplay(NULL);
  if (!x11_display) {
    printf("ERROR: XOpenDisplay failed.\n");
    return EXIT_FAILURE;
  }

  EGLDisplay egl_display = eglGetDisplay(x11_display);
  if (!egl_display) {
    printf("ERROR: eglGetDisplay failed.\n");
    return EXIT_FAILURE;
  }

  EGLint egl_major, egl_minor;
  if (!eglInitialize(egl_display, &egl_major, &egl_minor)) {
    printf("ERROR: eglInitialize failed.\n");
    return EXIT_FAILURE;
  }

  // Check if extension is available
  const char *extensions = eglQueryString(egl_display, EGL_EXTENSIONS);
  assert(strstr(extensions, "EGL_KHR_create_context_no_error"));

  const EGLint config_attribs[] = {
    EGL_RED_SIZE,   1,
    EGL_GREEN_SIZE, 1,
    EGL_BLUE_SIZE,  1,
    EGL_DEPTH_SIZE, 1,
    EGL_NONE
  };

  EGLConfig config;
  EGLint num_configs;
  if (!eglChooseConfig(egl_display, config_attribs, &config, 1, &num_configs)) {
    printf("ERROR: eglChooseConfig failed.\n");
    return false;
  }
  assert(config);
  assert(num_configs > 0);

  eglBindAPI(EGL_OPENGL_ES_API);

  if (!test_context(egl_display, config, 3, 2)) return EXIT_FAILURE;
  if (!test_context(egl_display, config, 2, 0)) return EXIT_FAILURE;
  if (!test_context(egl_display, config, 1, 1)) return EXIT_FAILURE;

  eglTerminate(egl_display);
  XCloseDisplay(x11_display);

  return EXIT_SUCCESS;
}
